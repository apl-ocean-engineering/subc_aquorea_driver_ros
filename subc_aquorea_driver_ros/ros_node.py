#!/usr/bin/env python3

import rospy
import serial
import threading
import time

from subc_aquorea_driver_ros.msg import AquoreaStatus
from subc_aquorea_driver_ros.aquorea import Aquorea

# from apl_msgs.msg import RawData


class AquoreaNode:
    def __init__(self) -> None:
        light_addresses = rospy.get_param("~lights", {"left": "SUBC19975"})
        port = rospy.get_param("~port", "")
        poll_interval = rospy.get_param("~poll_interval_sec", 5)

        try:
            self.port = serial.serial_for_url(port, timeout=1)
        except serial.SerialException:
            rospy.logfatal('Failed to initialize serial connection to port "%s"' % port)
            raise

        # Need to not try to send new commands while waiting for a status response
        # The lock is at the driver level, rather than the Sealite class level
        # since there may be multiple instances of the same class, all with different
        # addresses on the same port.
        self.port_lock = threading.Lock()

        self.light_map = {}
        for light, serial_number in light_addresses.items():
            rospy.loginfo(
                'Registering Sealite "%s" with serial number %d'
                % (light, serial_number)
            )
            self.light_map[light] = Aquorea(serial_number)

        #         rospy.Service("sealite_level", SealiteLevel, self.handle_sealite_level)
        #         rospy.Service(
        #             "sealite_increment", SealiteIncrement, self.handle_sealite_increment
        #         )

        self.status_pub = rospy.Publisher("status", AquoreaStatus, queue_size=10)

        rospy.Timer(rospy.Duration(float(poll_interval)), self.poll_lights)

        rospy.logdebug("Starting Aquorea driver")

    #     def handle_sealite_level(self, req: SealiteLevelRequest) -> SealiteLevelResponse:
    #         """
    #         Callback for service call that sets lights to input level.
    #         """
    #         active_lights = []
    #         levels = []
    #         # TODO: This would be cleaner if the message used an enum rather than freeform string
    #         if "all" in req.lights:
    #             if len(req.lights) != 1:
    #                 err_string = "If using light=all, request must be len 1"
    #                 rospy.logwarn(err_string)
    #                 return SealiteLevelResponse(-2, err_string)
    #             if len(req.lights) != len(req.level):
    #                 err_string = "req.lights must be same length as req.level"
    #                 rospy.logwarn(err_string)
    #                 return SealiteLevelResponse(-2, err_string)
    #             active_lights = list(self.light_map.keys())
    #             levels = [req.level[0] for _ in active_lights]
    #         else:
    #             active_lights = req.lights
    #             levels = req.level
    #         for light, level in zip(active_lights, levels):
    #             if light not in self.light_map:
    #                 err_string = 'Didn\'t understand light "%s"' % light
    #                 rospy.logwarn(err_string)
    #                 return SealiteLevelResponse(-2, err_string)

    #             rospy.loginfo('Setting light "%s" to level %d' % (light, level))
    #             with self.port_lock:
    #                 result = self.light_map[light].set_level(self.port, level)
    #             if not result:
    #                 err_string = "Failed to set level with Sealite {}".format(light)
    #                 rospy.logwarn(err_string)
    #                 return SealiteLevelResponse(-1, err_string)

    #         return SealiteLevelResponse(0, "")

    #     def handle_sealite_increment(
    #         self, req: SealiteIncrementRequest
    #     ) -> SealiteIncrementResponse:
    #         """
    #         Callback for service call that increments or decrements light levels.
    #         """
    #         # TODO(lindzey): This function (and handle_sealite_level) exits as soon as incrementing a single
    #         #    light fails. It might be better to append to an error string and
    #         #    then report that at the end.
    #         active_lights = []
    #         if "all" in req.lights:
    #             active_lights = list(self.light_map.keys())
    #         else:
    #             for light in req.lights:
    #                 if light not in self.light_map:
    #                     err_string = 'Didn\'t understand light "%s"' % light
    #                     rospy.logwarn(err_string)
    #                     return SealiteIncrementResponse(-2, err_string)
    #                 active_lights.append(light)

    #         for light in active_lights:
    #             if req.increment > 0:
    #                 rospy.loginfo('Incrementing light "%s" by %d' % (light, req.increment))
    #                 with self.port_lock:
    #                     result = self.light_map[light].increment_level(
    #                         self.port, req.increment
    #                     )
    #                 if not result:
    #                     err_string = "Failed to increment level with Sealite {}".format(
    #                         light
    #                     )
    #                     rospy.logwarn(err_string)
    #                     return SealiteIncrementResponse(-1, err_string)
    #             elif req.increment < 0:
    #                 delta = -1 * req.increment
    #                 rospy.loginfo('Decrementing light "%s" by %d' % (light, delta))
    #                 with self.port_lock:
    #                     result = self.light_map[light].decrement_level(self.port, delta)
    #                 if not result:
    #                     err_string = "Failed to increment level with Sealite {}".format(
    #                         light
    #                     )
    #                     rospy.logwarn(err_string)
    #                     return SealiteIncrementResponse(-1, err_string)

    #         return SealiteIncrementResponse(0, "")

    def poll_lights(self, _event: rospy.timer.TimerEvent) -> None:
        """
        Cycle through all lights, polling for temperature and brightness.
        Publish aggregate status as single ROS message.
        """
        t0 = time.time()
        rospy.logdebug("Polling light status")

        msg = AquoreaStatus()
        msg.header.stamp = rospy.Time.now()

        #         for light, sl in self.light_map.items():
        #             try:
        #                 with self.port_lock:
        #                     temp = sl.read_temperature(self.port)
        #             except DSPLTimeoutError:
        #                 rospy.logwarn("Timeout querying light \"%s\" temperature" % (light))
        #                 continue
        #             except DSPLNackError:
        #                 rospy.logwarn("NACK when querying light \"%s\" temperature" % (light))
        #                 continue
        #             except ValueError as err:
        #                 rospy.logwarn(
        #                     'Value error when querying light temperature "%s": %s'
        #                     % (light, err)
        #                 )
        #                 temp = -1

        #             try:
        #                 with self.port_lock:
        #                     level = sl.read_level(self.port)
        #                 if level is None:
        #                     rospy.logwarn("Could not read level. Setting to -1")
        #                     level = -1
        #             except DSPLTimeoutError:
        #                 rospy.logwarn("Timeout querying light \"%s\" level" % (light))
        #                 continue
        #             except DSPLNackError:
        #                 rospy.logwarn("NACK when querying light \"%s\" level" % (light))
        #                 continue

        #             rospy.logdebug("%s : temp = %s, level = %s" % (light, temp, level))

        #             msg.light.append(light)
        #             msg.address.append(sl.address)
        #             msg.level.append(level)
        #             msg.temperature.append(temp)

        self.status_pub.publish(msg)
        dt = time.time() - t0
        rospy.logdebug(
            "Polling {} lights required {:.04f} seconds.".format(
                len(self.light_map), dt
            )
        )


# if __name__ == "__main__":
#     try:
#         rospy.init_node("dspl_sealite_driver")
#         SealiteNode()
#         rospy.spin()
#     except rospy.ROSInterruptException:
#         pass
