# aquorea_driver_ros

ROS driver for one or more serial-controlled
[SubC]()
[Aquorea]() on a
single serial bus.

Built on [pydspl_seasense](https://gitlab.com/apl-ocean-engineering/pydspl_seasense)

The `subc_aquorea_driver` node takes the following parameters:

 * `~port`: File path or [URL](https://pyserial.readthedocs.io/en/latest/url_handlers.html)
 for the serial port.  `pydspl_seasense` is built on `pyserial`, so any
 `serial.serial_for_url()` should be valid.   Required, driver will not start
 if unset.

 * `~poll_interval_sec`:  Interval for driver polling of light status, in seconds.  If set
 to 0, polling is disabled.   Defaults to 5 seconds.

 * `~lights`: A map in the form of `{light_1: <addr1>, light_2: <addr2> }` which
 maps light names to Seasense addresses.   Light names must be unique.   A special name
 `all` is always defined which results in commands being sent to all lights ---
 this is done by looping through each light in the map *not* by using
 the Seasense protocol broadcast functionality.

## Messages

The `AquoreaStatus` message describes the current state of one light.

```
std_msgs/Header header
string light_name
string serial_number
float32 temperature
int8 level
```

`light_name` is the name of the light, `serial_number` is the Seasense address for the
light.  `temperature` is the reported temperature in degrees C, and `level`
is the current level (0-100).

The list of lights known to the driver can be introspected by observing the
set of `AquoreaStatus` messages.


# Debugging

With a serial-to-USB adapter on `/dev/ttyUSB0`:

```
picocom -b 115200 /dev/ttyUSB0 --omap crlf --imap lfcrlf
```

# Aquorea API

All commands start with a tilde (`~`) and end with a LF.  Responses from the light end in a LF.

In multi-drop configurations, a light can be address by adding a pipe `|` and the serial number of the light e.g. `~Aux0SetEnable|SUBC15000`

Our lights defaulted to 115200 baud.

| Command | Description |
|---------|-------------|
